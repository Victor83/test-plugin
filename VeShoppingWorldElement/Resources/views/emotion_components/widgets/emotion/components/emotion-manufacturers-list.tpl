{block name="widgets_emotion_components_manufacturers_list_element"}

    <h3>Manufacturers list</h3>
    <ol>
        {foreach from=$Data.manufacturers item=manufacturer}
            <li>{$manufacturer->getName()}</li>
        {/foreach}
    </ol>

{/block}