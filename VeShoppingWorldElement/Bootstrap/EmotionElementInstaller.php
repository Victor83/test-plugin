<?php

namespace VeShoppingWorldElement\Bootstrap;

use Shopware\Components\Emotion\ComponentInstaller;

class EmotionElementInstaller
{
    /** @var EmotionElementInstaller $emotionComponentInstaller */
    private $emotionComponentInstaller;

    /** @var string $pluginName */
    private $pluginName;

    /**
     * VeShoppingWorldElementInstaller constructor.
     * @param string $pluginName
     * @param ComponentInstaller $emotionComponentInstaller
     */
    public function __construct($pluginName, ComponentInstaller $emotionComponentInstaller)
    {
        $this->emotionComponentInstaller = $emotionComponentInstaller;
        $this->pluginName = $pluginName;
    }

    public function install()
    {
        $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'VeShoppingWorldElement',
            [
                'name' => 'Manufacturer\'s list element',
                'xtype' => 'emotion-components-manufacturers-list',
                'template' => 'emotion-manufacturers-list',
                'cls' => 'manufacturer-list-element',
                'description' => 'Manufacturer\'s list for Shopping world',
            ]
        );
    }
}