<?php

namespace VeShoppingWorldElement\ComponentHandler;

use Shopware\Bundle\EmotionBundle\ComponentHandler\ComponentHandlerInterface;
use Shopware\Bundle\EmotionBundle\Struct\Collection\PrepareDataCollection;
use Shopware\Bundle\EmotionBundle\Struct\Collection\ResolvedDataCollection;
use Shopware\Bundle\EmotionBundle\Struct\Element;
use Shopware\Bundle\StoreFrontBundle\Struct\ShopContextInterface;

class ManufacturersListComponentHandler implements ComponentHandlerInterface
{

    /**
     * @param Element $element
     * @return bool
     */
    public function supports(Element $element)
    {
        return $element->getComponent()->getTemplate() == 'emotion-manufacturers-list';
    }

    public function prepare(PrepareDataCollection $collection, Element $element, ShopContextInterface $context)
    {
    }

    public function handle(ResolvedDataCollection $collection, Element $element, ShopContextInterface $context)
    {
        $manufacturers = Shopware()->Models()->getRepository('Shopware\Models\Article\Supplier')->findAll();

        $element->getData()->set('manufacturers', $manufacturers);
    }
}